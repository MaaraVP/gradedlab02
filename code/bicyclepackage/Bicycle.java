// Maara Vanessa Purici
// Student ID: 2132640

package bicyclepackage;

public class Bicycle {
    // Creating the 3 private fields
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    // Creating ge methods for all 3 fields
    public String getManufacturer(){
        return this.manufacturer;
    }

    public int getNumberGears(){
        return this.numberGears;
    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    // Creating a constructor
    public Bicycle(String givenManufacturer, int givenNumberGears, double givenMaxSpeed){
        this.manufacturer = givenManufacturer;
        this.numberGears = givenNumberGears;
        this.maxSpeed = givenMaxSpeed;
    }

    // Creating a toString() method
    public String toString(){
        String toReturn = "Manufacturer: " +this.manufacturer+ ", Number of Gears: " +this.numberGears+ ", MaxSpeed: " +this.maxSpeed;
        return toReturn;
    }
}
