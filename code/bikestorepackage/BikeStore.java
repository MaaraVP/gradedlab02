// Maara Vanessa Purici
// Student ID: 2132640

package bikestorepackage;
import bicyclepackage.Bicycle;

public class BikeStore {
    public static void main(String[] args){
        Bicycle[] vehicles = new Bicycle[4];

        vehicles[0] = new Bicycle("Argon 18", 18, 30.0);

        vehicles[1] = new Bicycle("Brodie Bicycles", 24, 40.0);

        vehicles[2] = new Bicycle("Berlin & Racycle Manufacturing Company", 27, 35.0);
        
        vehicles[3] = new Bicycle("CCM", 30, 45.0);
       
        for(int i = 0; i< vehicles.length; i++){
            System.out.println();
            System.out.println(vehicles[i]);
        }
    }
}
